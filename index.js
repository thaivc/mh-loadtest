const SockJS = require('sockjs-client')
const Stomp  = require('stompjs')
const express = require('express')
const app = express()


app.get('/', function (req, res) {
  res.send('Hello World')
})

app.listen(3000)

let loop = 1000;
const connectSocket = () => {
  // const link = "'http://172.66.2.60:8088/stomp";
  // const link = 'http://8.209.245.33:8088/stomp';
  // const link = 'https://stg-api.metahorse.racing';
  // const link = 'http://localhost:8088/stomp';
  const link = 'https://dev-api-meta-horse.bappartners.com/stomp'
  return new Promise(function (resolve, reject) {
    const stomp = Stomp.over(new SockJS(link))
    stomp.connect(
      {},
      () => {
        resolve(stomp)
      },
      (err) => {
        reject(err)
      }
    )
  })
}

const onReceivedData = (data, i) => {
  console.log(`Data socket - ${i} with current frame: ${data.frame} in ${new Date()}`)
}

const sleep = (n) => { return new Promise(resolve => setTimeout(resolve,n )) }

//LOCAL
(async () => {
  for (let i = 0; i < 10; i++) {
    for (let j = 0; j < 100;j ++) {
      connectSocket().then((client) => {
        const raceId = (Math.random() * 20 + 20) >>> 0
        // const raceId = 4;
        console.log(`connected ${raceId} - ${i} in ${new Date()}`)
        client.send('/app/live-race/current-data', {}, raceId)
        client.subscribe(`/user/topic/live-race/current-data/${raceId}`, (message) => {
          // console.log(message.body, 'message.body')
          const frameData = JSON.parse(message.body)
          onReceivedData(frameData, raceId)
        })
        client.subscribe(`/topic/live-race/${raceId}`, (message) => {
          const frameData = JSON.parse(message.body)
          onReceivedData(frameData, raceId)
        })
      },
      (err) => {
        console.log(err)
      })
    }
    await sleep(5_000);

  }
})();


// REMOTE
// (async () => {
//   try {
//     for (let i = 0; i < loop; i++) {

  //     connectSocket().then((client) => {
  //       const raceId = (Math.random() * 20 + 20) >>> 0
  //       // const raceId = 4;
  //       console.log(`connected ${raceId} - ${i} in ${new Date()}`)
  //       client.send('/app/live-race/current-data', {}, raceId)
  //       client.subscribe(`/user/topic/live-race/current-data/${raceId}`, (message) => {
  //         // console.log(message.body, 'message.body')
  //         const frameData = JSON.parse(message.body)
  //         onReceivedData(frameData, raceId)
  //       })
  //       client.subscribe(`/topic/live-race/${raceId}`, (message) => {
  //         const frameData = JSON.parse(message.body)
  //         onReceivedData(frameData, raceId)
  //       })
  //     },
  //     (err) => {
  //       console.log(err)
  //     })
  //   }
  // } catch(err) {
  //   console.log(err)
  // }
// })();
